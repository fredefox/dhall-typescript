module Main where

import qualified Dhall.TypeScript

main :: IO ()
main = Dhall.TypeScript.main
