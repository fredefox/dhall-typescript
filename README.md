# dhall-typescript

    $ dhall-typescript <<< '< a : Integer | b : Text | c : { m : Natural } >'
    { a: number; } | { b: string; } | { c: { m: number; }; }

